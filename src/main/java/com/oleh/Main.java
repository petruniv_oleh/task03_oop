package com.oleh;

import com.oleh.view.View;

/**
 * Entrance point
 * @author Oleh Petruniv
 */
public class Main {

    public static void main(String[] args) {
        View view = new View();
        view.showMenu();
    }
}
