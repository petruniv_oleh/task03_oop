package com.oleh.model.homeObj;

import java.util.List;

/**
 * This is basic class for flat
 */
public class Flat extends Home {

    private HomeType homeType;
    private int floorNumber;
    private boolean haveConcierge;
    private boolean haveLift;

    /**
     * <p>Constructor for Flat</p>
     * @param size - the size of flat
     * @param price - the price of flat
     * @param address - the address of flat
     * @param infrastures - the infrastructures nearby
     * @param homeType - the type of flat
     * @param floorNumber - the floor number of flat
     * @param haveConcierge - availability of concierge
     * @param haveLift - availability of lift
     */
    public Flat(int size, int price, String address, List<Infrastructure> infrastures,
                HomeType homeType, int floorNumber, boolean haveConcierge, boolean haveLift) {
        super(size, price, address, infrastures);
        this.homeType = homeType;
        this.floorNumber = floorNumber;
        this.haveConcierge = haveConcierge;
        this.haveLift = haveLift;
    }

    /**
     * overrided parental method
     * @return
     */
    @Override
    public HomeType getHomeType() {
        return homeType;
    }

    public void setHomeType(HomeType homeType) {
        this.homeType = homeType;
    }

    public int getFloorNumber() {
        return floorNumber;
    }

    public void setFloorNumber(int floorNumber) {
        this.floorNumber = floorNumber;
    }

    public boolean isHaveConcierge() {
        return haveConcierge;
    }

    public void setHaveConcierge(boolean haveConcierge) {
        this.haveConcierge = haveConcierge;
    }

    public boolean isHaveLift() {
        return haveLift;
    }

    public void setHaveLift(boolean haveLift) {
        this.haveLift = haveLift;
    }

    @Override
    public String toString() {
        return "Flat{" + super.toString()+
                ", homeType=" + homeType +
                ", floorNumber=" + floorNumber +
                ", haveConcierge=" + haveConcierge +
                ", haveLift=" + haveLift +
                '}';
    }
}
