package com.oleh.model;

import com.oleh.model.homeObj.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

/**
 * Model class, where all business logic is
 *
 * @author Oleh Petruniv
 */
public class Model implements ModelInt {
    List<Home> homeList;

    public Model() {
        homeList = new ArrayList<Home>();
        fillTheList();
    }

    private void fillTheList() {
        String[] streats = {"вул. Хуторівка", "вул. Стрийська",
                "вул. Площа ринок", "вул. Сахарова", "вул. С.Бандери"};
        InfrastructureType[] infras = {InfrastructureType.KINDERGARTEN, InfrastructureType.LIBRARY,
                InfrastructureType.PLAYGROUND, InfrastructureType.SCHOOL};

        Random random = new Random();
        int amountOfhomes = random.nextInt(10) + 5;
        for (int i = 0; i < amountOfhomes; i++) {
            int amountOfInfas = random.nextInt(3) + 1;
            List<Infrastructure> infrasList = new ArrayList<Infrastructure>();
            for (int j = 0; j < amountOfInfas; j++) {
                int randomInfra = random.nextInt(4);
                infrasList.add(new Infrastructure(infras[randomInfra], random.nextInt(3) + 1));
            }
            int homeType = random.nextInt(3);
            switch (homeType) {
                case 0:
                    homeList.add(new Flat(random.nextInt(300) + 50, random.nextInt(500) + 100,
                            streats[random.nextInt(5)], infrasList, HomeType.FLAT,
                            random.nextInt(15) + 1, random.nextBoolean(), random.nextBoolean()));
                    break;
                case 1:
                    homeList.add(new Flat(random.nextInt(300) + 120, random.nextInt(1000) + 300,
                            streats[random.nextInt(5)], infrasList, HomeType.PENTHOUSE,
                            random.nextInt(15) + 1, random.nextBoolean(), random.nextBoolean()));
                    break;
                case 2:
                    homeList.add(new Mansione(random.nextInt(300) + 200, random.nextInt(1000) + 500,
                            streats[random.nextInt(5)], infrasList, random.nextInt(3) + 1));
                    break;
                default:
                    break;
            }

        }


    }

    public List<Home> getAll() {
        return homeList;
    }

    public List<InfrastructureType> getInfras(List<Home> homes) {
        List<InfrastructureType> infras = new ArrayList<InfrastructureType>();
        for (Home home : homes
        ) {
            List<Infrastructure> infrastructures = home.getInfrastructures();
            for (int i = 0; i < infrastructures.size(); i++) {
                if (!isInfraInList(infras, infrastructures.get(i).getInfrastructureType())) {
                    infras.add(infrastructures.get(i).getInfrastructureType());
                }
            }
        }

        return infras;
    }

    public List<Home> findAndSort(int price, InfrastructureType infrastructureType, int distanceToInfr) {
        List<Home> result = new ArrayList<Home>();
        for (Home home : homeList) {
            if (home.haveInfra(infrastructureType) && (home.getPrice() <= price)
                    && (home.getDistanceToInfra(infrastructureType) <= distanceToInfr)) {
                result.add(home);
            }
        }
        return result;
    }

    private boolean isInfraInList(List<InfrastructureType> infrastructures, InfrastructureType infrastructureType) {
        for (int i = 0; i < infrastructures.size(); i++) {
            if (infrastructures.get(i).equals(infrastructureType)) {
                return true;
            }
        }
        return false;
    }

    public List<Home> getHomesByPrice(int price) {
        List<Home> result = new ArrayList<Home>();
        for (int i = 0; i < homeList.size(); i++) {
            if (homeList.get(i).getPrice() <= price) {
                result.add(homeList.get(i));
            }
        }

        return result;
    }

    public void sortByPrice() {
        homeList.sort(new Comparator<Home>() {
            public int compare(Home o1, Home o2) {
                return o1.getPrice() - o2.getPrice();
            }
        });
    }
}
