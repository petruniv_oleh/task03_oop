package com.oleh.model.homeObj;

public enum InfrastructureType {
    SCHOOL, KINDERGARTEN, PLAYGROUND, LIBRARY;
}
