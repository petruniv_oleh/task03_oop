package com.oleh.model;

import com.oleh.model.homeObj.Home;
import com.oleh.model.homeObj.InfrastructureType;

import java.util.List;

public interface ModelInt {
    List<Home> getAll();
    List<Home> findAndSort(int price, InfrastructureType infrastructureType, int distanceToInfr);
    List<InfrastructureType> getInfras(List<Home> homes);
    List<Home> getHomesByPrice(int price);

    void sortByPrice();





}
