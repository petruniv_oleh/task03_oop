package com.oleh.model.homeObj;

import java.util.List;

public class Mansione extends Home {
    private HomeType homeType;
    private int amountOfFloors;

    public Mansione(int size, int price, String address, List<Infrastructure> infrastructures,
                    int amountOfFloors) {
        super(size, price, address, infrastructures);
        this.homeType = HomeType.MANSION;
        this.amountOfFloors = amountOfFloors;
    }

    @Override
    public HomeType getHomeType() {
        return homeType;
    }

    public void setHomeType(HomeType homeType) {
        this.homeType = homeType;
    }

    public int getAmountOfFloors() {
        return amountOfFloors;
    }

    public void setAmountOfFloors(int amountOfFloors) {
        this.amountOfFloors = amountOfFloors;
    }

    @Override
    public String toString() {
        return "Mansione{" + super.toString() +
                ", homeType=" + homeType +
                ", amountOfFloors=" + amountOfFloors +
                '}';
    }
}
