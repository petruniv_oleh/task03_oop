package com.oleh.view;

import com.oleh.controller.Controller;
import com.oleh.model.homeObj.Home;
import com.oleh.model.homeObj.InfrastructureType;

import java.util.List;
import java.util.Scanner;

/**
 * View class where is user interface
 * @author Oleh Petruniv
 */
public class View implements ViewInt {
    private Controller controller;


    public View() {
        controller = new Controller();
        showMenu();
    }

    public void showMenu() {
        Scanner sc = new Scanner(System.in, "UTF-8");
        System.out.println("\n Menu: ");
        System.out.println("==================");
        System.out.println("1. Print all homes");
        System.out.println("2. Search homes");
        System.out.println("3. Exit");
        System.out.println("==================");
        System.out.println("Enter a value: ");
        switch (sc.nextInt()) {
            case 1:
                printAll();
                break;
            case 2:
                search();
                break;
            case 3:
                Runtime.getRuntime().exit(0);
                break;
            default:
                break;
        }

    }


    public void printAll() {
        List<Home> all = controller.getAll();
        for (int i = 0; i < all.size(); i++) {
            System.out.println(all.get(i));
        }
        showMenu();
    }

    public void search() {
        Scanner sc;
        sc = new Scanner(System.in, "UTF-8");
        InfrastructureType infrastructureType;
        System.out.print("Max price: ");
        int price = sc.nextInt();

        List<InfrastructureType> infrasType = controller.getInfrasType(price);
        if (infrasType.size() == 0) {
            System.out.println("There is no homes for this price.");
            return;
        } else {
            System.out.println("types of infrastructure: ");
            for (int i = 0; i < infrasType.size(); i++) {
                System.out.println("" + (i + 1) + ". " + infrasType.get(i));
            }
        }
        System.out.println("Choose one: ");
        infrastructureType = infrasType.get(sc.nextInt() - 1);
        System.out.print("Max distance to it: ");
        int distance = sc.nextInt();
        List<Home> resultList = controller.findBy(price, infrastructureType, distance);
        if (resultList.size() == 0) {
            System.out.println("There is no homes for your request");
        } else {
            System.out.println("Search results. Find " + resultList.size() + " homes");
            for (Home home : resultList
            ) {
                System.out.println(home);
            }
        }

        showMenu();
    }
}
