package com.oleh.model.homeObj;

public enum HomeType {
    MANSION, FLAT, PENTHOUSE;
}
