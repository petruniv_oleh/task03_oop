package com.oleh.controller;

import com.oleh.model.Model;
import com.oleh.model.homeObj.Home;
import com.oleh.model.homeObj.InfrastructureType;

import java.util.List;

/**
 * The controller class
 * @author Oleh Petruniv
 */
public class Controller implements ControllerInt {
    private Model model;

    public Controller() {
        model = new Model();
    }

    public List<Home> getAll() {
        model.sortByPrice();
        return model.getAll();
    }

    public List<Home> findBy(int price, InfrastructureType infrastructureType, int distanceToInfr) {
        model.sortByPrice();
        return model.findAndSort(price, infrastructureType, distanceToInfr);
    }

    public List<InfrastructureType> getInfrasType(int price) {
        return model.getInfras(model.getHomesByPrice(price));

    }
}
