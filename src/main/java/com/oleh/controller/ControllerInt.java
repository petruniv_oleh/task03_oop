package com.oleh.controller;

import com.oleh.model.homeObj.Home;
import com.oleh.model.homeObj.InfrastructureType;

import java.util.List;

public interface ControllerInt {
    List<Home> getAll();
    List<InfrastructureType> getInfrasType(int price);
    List<Home> findBy(int price, InfrastructureType infrastructureType, int distanceToInfr);

}
