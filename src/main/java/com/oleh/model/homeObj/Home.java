package com.oleh.model.homeObj;

import java.util.List;

/**
 * the parent class for all homes
 */
public class Home {
    private int size;
    private int price;
    private String address;
    private List<Infrastructure> infrastructures;

    public Home(int size, int price, String address,
                List<Infrastructure> infrastructures) {
        this.size = size;
        this.price = price;
        this.address = address;
        this.infrastructures = infrastructures;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Infrastructure> getInfrastructures() {
        return infrastructures;
    }

    public void setInfrastructures(List<Infrastructure> infrastructures) {
        this.infrastructures = infrastructures;
    }

    /**
     * @param infrastructureType
     * @return availability of infrastructure
     */
    public boolean haveInfra(InfrastructureType infrastructureType) {
        for (int i = 0; i < infrastructures.size(); i++) {
            if (infrastructures.get(i).getInfrastructureType().equals(infrastructureType)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param infrastructureType - the type of infrastructure
     * @return distance to infrastructure
     */
    public int getDistanceToInfra(InfrastructureType infrastructureType) {
        for (int i = 0; i < infrastructures.size(); i++) {
            if (infrastructures.get(i).getInfrastructureType().equals(infrastructureType)) {
                return infrastructures.get(i).getDistance();
            }
        }
        return 0;
    }


    public HomeType getHomeType() {
        return null;
    }


    @Override
    public String toString() {
        return "Home{" +
                "size=" + size +
                ", price=" + price +
                ", address='" + address + '\'' +
                ", infrastures=" + infrastructures.toString() +
                '}';
    }
}
