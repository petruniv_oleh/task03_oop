package com.oleh.model.homeObj;

public class Infrastructure {
    private InfrastructureType infrastructureType;
    private int distance;

    public Infrastructure(InfrastructureType infrastructureType, int distance) {
        this.infrastructureType = infrastructureType;
        this.distance = distance;
    }

    public InfrastructureType getInfrastructureType() {
        return infrastructureType;
    }

    public void setInfrastructureType(InfrastructureType infrastructureType) {
        this.infrastructureType = infrastructureType;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Infrastructure{" +
                "infrastructureType=" + infrastructureType +
                ", distance=" + distance +
                '}';
    }
}
